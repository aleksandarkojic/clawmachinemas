//
//  MTRGMediationAdapter.h
//  myTargetSDK 5.9.6
//
// Copyright (c) 2019 Mail.Ru Group. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol MTRGMediationAdapter <NSObject>

- (void)destroy;

@end

NS_ASSUME_NONNULL_END
